# advent-of-code-2022

See <https://adventofcode.com/>

## Installation

```sh
asdf install
mix local.rebar
mix local.hex
mix deps.get
```

## Usage

- Tests: `mix check`
- Store input file locally: `mix fetch 01`
- Run: `mix solve 01` or `mix solve 01 --part2`
