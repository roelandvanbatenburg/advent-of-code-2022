defmodule CathodeRay do
  @moduledoc """
  Watch something on the tube.
  """
  defmodule PartOne do
    @moduledoc "First puzzle."
    @doc "Solve the first puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> CathodeRay.parse()
      |> find_signal_strength()
    end

    defp find_signal_strength(cycles) do
      {_register, signal_strength} =
        1..length(cycles)
        |> Enum.reduce({1, 0}, fn i, {register, signal_strength} ->
          signal_strength = maybe_update_signal_strength(i, register, signal_strength)

          case Enum.at(cycles, i - 1) do
            :noop -> {register, signal_strength}
            {:add, x} -> {register + x, signal_strength}
          end
        end)

      signal_strength
    end

    defp maybe_update_signal_strength(i, _register, signal) when rem(i - 20, 40) != 0, do: signal
    defp maybe_update_signal_strength(i, register, signal), do: signal + i * register
  end

  defmodule PartTwo do
    @moduledoc "Second puzzle."
    @doc "Solve the second puzzle."
    @spec run(String.t()) :: String.t()
    def run(input) do
      input
      |> CathodeRay.parse()
      |> render_display()
    end

    defp render_display(cycles) do
      {_register, display} =
        0..(length(cycles) - 1)
        |> Enum.reduce({1, []}, fn i, {register, display} ->
          display = add_pixel(rem(i, 40), register, display)

          case Enum.at(cycles, i) do
            :noop -> {register, display}
            {:add, x} -> {register + x, display}
          end
        end)

      display
      |> Enum.reverse()
      |> Enum.chunk_every(40)
      |> Enum.map_join("\n", &Enum.join/1)
    end

    defp add_pixel(i, register, display) when i < register - 1, do: ["." | display]
    defp add_pixel(i, register, display) when i > register + 1, do: ["." | display]
    defp add_pixel(_i, _register, display), do: ["#" | display]
  end

  @type instructions :: [:noop | {:add, integer()}]

  @doc "Parse the program."
  @spec parse(String.t()) :: instructions
  def parse(input) do
    input
    |> String.split("\n")
    |> Enum.flat_map(fn
      "noop" -> [:noop]
      "addx " <> x -> [:noop, {:add, String.to_integer(x)}]
    end)
  end
end
