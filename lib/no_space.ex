defmodule NoSpace do
  @moduledoc """
  Make room.
  """
  defmodule PartOne do
    @moduledoc "First puzzle."
    @doc "Solve the first puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> NoSpace.parse()
      |> NoSpace.get_dirs_size()
      |> get_small_dirs_size()
    end

    defp get_small_dirs_size(dirs) do
      Enum.reduce(dirs, 0, fn {_path, size}, total_size ->
        if size > 100_000 do
          total_size
        else
          total_size + size
        end
      end)
    end
  end

  defmodule PartTwo do
    @moduledoc "Second puzzle."
    @doc "Solve the second puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> NoSpace.parse()
      |> NoSpace.get_dirs_size()
      |> find_disk_to_delete()
    end

    defp find_disk_to_delete(dirs) do
      disk_usage = Map.fetch!(dirs, "/")
      target = 30_000_000 - (70_000_000 - disk_usage)
      Enum.reduce(dirs, nil, fn {_dir, size}, current -> match(current, size, target) end)
    end

    defp match(nil, size, target) when size < target, do: nil
    defp match(nil, size, _target), do: size
    defp match(current, size, target) when size < target, do: current
    defp match(current, size, _target) when size < current, do: size
    defp match(current, _size, _target), do: current
  end

  @doc "Parse the commands and output."
  @spec parse(String.t()) :: map()
  def parse(input) do
    input
    |> String.split("\n")
    |> Stream.map(&String.trim/1)
    |> Stream.map(&tokenize/1)
    |> Enum.to_list()
    |> do_parse(["/"], %{"/" => %{}})
  end

  defp tokenize(line) do
    cond do
      line == "$ ls" -> :ls
      line == "$ cd /" -> :root
      line == "$ cd .." -> :dirup
      result = Regex.run(~r/^\$ cd (.*)$/i, line) -> {:chdir, Enum.at(result, 1)}
      result = Regex.run(~r/^dir (.+)$/i, line) -> {:subdir, Enum.at(result, 1)}
      result = Regex.run(~r/^(\d+) (.+)$/i, line) -> tokenize_file(result)
    end
  end

  defp tokenize_file(result) do
    {
      :file,
      Enum.at(result, 2),
      result |> Enum.at(1) |> String.to_integer()
    }
  end

  defp do_parse(tokens, path, filesystem)
  defp do_parse([], _path, filesystem), do: filesystem
  defp do_parse([:ls | tokens], path, filesystem), do: do_parse(tokens, path, filesystem)
  defp do_parse([:root | tokens], _path, filesystem), do: do_parse(tokens, ["/"], filesystem)
  defp do_parse([:dirup | tokens], [_subdir | path], fs), do: do_parse(tokens, path, fs)

  defp do_parse([{:chdir, subdir} | tokens], path, filesystem) do
    path = [subdir | path]
    do_parse(tokens, path, init_dir(filesystem, path))
  end

  defp do_parse([{:subdir, subdir} | tokens], path, filesystem),
    do: do_parse(tokens, path, init_dir(filesystem, [subdir | path]))

  defp do_parse([{:file, file, size} | tokens], path, filesystem) do
    filesystem = put_in(filesystem, Enum.reverse([file | path]), size)
    do_parse(tokens, path, filesystem)
  end

  defp init_dir(filesystem, path) do
    {_value, filesystem} =
      get_and_update_in(filesystem, Enum.reverse(path), fn
        nil -> {nil, %{}}
        v -> {v, v}
      end)

    filesystem
  end

  @doc "Get size per directory."
  @spec get_dirs_size(map(), map(), String.t()) :: %{required(String.t()) => integer()}
  def get_dirs_size(filesystem, dirs \\ %{}, path \\ "") do
    Enum.reduce(filesystem, dirs, fn
      {_file, size}, dirs when is_integer(size) -> dirs
      {dir, subdirs}, dirs -> add_dir_and_subdir_sizes(dir, subdirs, path, dirs)
    end)
  end

  defp add_dir_and_subdir_sizes(dir, subdirs, path, dirs) do
    size = size(subdirs)
    path = path(path, dir)
    dirs = Map.put(dirs, path, size)
    get_dirs_size(subdirs, dirs, path)
  end

  defp size(dir) do
    Enum.reduce(dir, 0, fn
      {_dir, subdirs}, total_size when is_map(subdirs) -> total_size + size(subdirs)
      {_file, size}, total_size -> total_size + size
    end)
  end

  defp path("", dir), do: dir
  defp path("/", dir), do: "/#{dir}"
  defp path(path, dir), do: "#{path}/#{dir}"
end
