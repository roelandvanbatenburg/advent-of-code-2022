defmodule RucksackReorganization do
  @moduledoc """
  Organize the Rucksacks.
  """
  defmodule PartOne do
    @moduledoc "First puzzle."
    @doc "Solve the first puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> String.split()
      |> Enum.reduce(0, fn rucksack, acc ->
        {c1, c2} = items_per_compartment(rucksack)

        MapSet.intersection(c1, c2)
        |> MapSet.to_list()
        |> List.first()
        |> RucksackReorganization.score()
        |> Kernel.+(acc)
      end)
    end

    defp items_per_compartment(rucksack) do
      total_items = String.length(rucksack)
      items_per_compartment = div(total_items, 2)

      {
        parse_compartment(rucksack, 0, items_per_compartment),
        parse_compartment(rucksack, items_per_compartment, total_items)
      }
    end

    defp parse_compartment(rucksack, start, length) do
      rucksack
      |> String.slice(start, length)
      |> RucksackReorganization.mapset()
    end
  end

  defmodule PartTwo do
    @moduledoc "Second puzzle."
    @doc "Solve the second puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> String.split()
      |> Enum.chunk_every(3)
      |> Enum.reduce(0, fn rucksacks, acc ->
        rucksacks
        |> Enum.map(&RucksackReorganization.mapset/1)
        |> Enum.reduce(&MapSet.intersection/2)
        |> MapSet.to_list()
        |> List.first()
        |> RucksackReorganization.score()
        |> Kernel.+(acc)
      end)
    end
  end

  @doc "Make a `MapSet` from (part of) a rucksack."
  @spec mapset(String.t()) :: MapSet.t()
  def mapset(rucksack) do
    rucksack
    |> String.split("", trim: true)
    |> MapSet.new()
  end

  @doc "Calculate the score of a single item."
  @spec score(String.t()) :: integer
  def score(<<lower::utf8>>) when lower in ?a..?z, do: lower - ?a + 1
  def score(<<upper::utf8>>) when upper in ?A..?Z, do: upper - ?A + 27
end
