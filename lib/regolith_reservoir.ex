defmodule RegolithReservoir do
  @moduledoc """
  Drop the sand.
  """
  defmodule PartOne do
    @moduledoc "First puzzle."
    @doc "Solve the first puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> RegolithReservoir.parse()
      |> drop_the_sand()
    end

    defp drop_the_sand(cave, max_y \\ nil, sand_count \\ 0) do
      max_y = max_y(cave, max_y)

      case rest_or_void(cave, max_y, {500, 0}) do
        :void ->
          sand_count

        location ->
          cave
          |> Map.put(location, :sand)
          |> drop_the_sand(max_y, sand_count + 1)
      end
    end

    defp max_y(cave, nil) do
      cave
      |> Enum.reduce(0, fn {{_x, y}, _}, max_y ->
        max(y, max_y)
      end)
    end

    defp max_y(_cave, max_y), do: max_y

    defp rest_or_void(_cave, max_y, {_x, max_y}), do: :void

    defp rest_or_void(cave, max_y, {x, y}) do
      cond do
        not Map.has_key?(cave, {x, y + 1}) -> rest_or_void(cave, max_y, {x, y + 1})
        not Map.has_key?(cave, {x - 1, y + 1}) -> rest_or_void(cave, max_y, {x - 1, y + 1})
        not Map.has_key?(cave, {x + 1, y + 1}) -> rest_or_void(cave, max_y, {x + 1, y + 1})
        true -> {x, y}
      end
    end
  end

  defmodule PartTwo do
    @moduledoc "Second puzzle."
    @doc "Solve the second puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> RegolithReservoir.parse()
      |> drop_the_sand()
    end

    defp drop_the_sand(cave, max_y \\ nil, sand_count \\ 0) do
      max_y = max_y(cave, max_y)

      rest_spot = rest_spot(cave, max_y, {500, 0})

      if rest_spot == {500, 0} do
        sand_count + 1
      else
        cave
        |> Map.put(rest_spot, :sand)
        |> drop_the_sand(max_y, sand_count + 1)
      end
    end

    defp max_y(cave, nil) do
      cave
      |> Enum.reduce(0, fn {{_x, y}, _}, max_y ->
        max(y, max_y)
      end)
      |> Kernel.+(2)
    end

    defp max_y(_cave, max_y), do: max_y

    defp rest_spot(cave, max_y, {x, y}) do
      cond do
        free?(cave, {x, y + 1}, max_y) -> rest_spot(cave, max_y, {x, y + 1})
        free?(cave, {x - 1, y + 1}, max_y) -> rest_spot(cave, max_y, {x - 1, y + 1})
        free?(cave, {x + 1, y + 1}, max_y) -> rest_spot(cave, max_y, {x + 1, y + 1})
        true -> {x, y}
      end
    end

    defp free?(_cave, {_x, max_y}, max_y), do: false
    defp free?(cave, location, _max_y), do: not Map.has_key?(cave, location)
  end

  @type location :: {integer, integer}
  @type cave :: %{required(location()) => :rock | :sand}

  @doc "Parse the cave map."
  @spec parse(String.t()) :: cave()
  def parse(input) do
    input
    |> String.split("\n")
    |> Enum.reduce(%{}, fn line, cave ->
      {cave, _} =
        line
        |> String.split(" -> ")
        |> Enum.map(&String.split(&1, ","))
        |> Enum.reduce({cave, nil}, &add_rocks/2)

      cave
    end)
  end

  defp add_rocks([x, y], {cave, nil}), do: {cave, {String.to_integer(x), String.to_integer(y)}}

  defp add_rocks([x_string, y_string], {cave, {x_start, y_start}}) do
    x_finish = String.to_integer(x_string)
    y_finish = String.to_integer(y_string)

    cave =
      if x_finish == x_start do
        y_start..y_finish
        |> Enum.reduce(cave, fn y, cave -> Map.put(cave, {x_start, y}, :rock) end)
      else
        x_start..x_finish
        |> Enum.reduce(cave, fn x, cave -> Map.put(cave, {x, y_start}, :rock) end)
      end

    {cave, {x_finish, y_finish}}
  end
end
