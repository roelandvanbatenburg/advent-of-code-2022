defmodule CaloryCounting do
  @moduledoc """
  Count the calories
  """
  defmodule PartOne do
    @moduledoc "First puzzle"
    @doc "Solve the first puzzle"
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> CaloryCounting.get_calories_per_elf()
      |> Enum.max()
    end
  end

  defmodule PartTwo do
    @moduledoc "Second puzzle"
    @doc "Solve the second puzzle"
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> CaloryCounting.get_calories_per_elf()
      |> Enum.sort(:desc)
      |> Enum.take(3)
      |> Enum.sum()
    end
  end

  @doc "Return a list of calory totals"
  @spec get_calories_per_elf(String.t()) :: [integer()]
  def get_calories_per_elf(input) do
    input
    |> String.split("\n\n")
    |> Enum.map(fn one_elf_text ->
      one_elf_text
      |> String.split("\n")
      |> Enum.map(&String.to_integer/1)
      |> Enum.sum()
    end)
  end
end
