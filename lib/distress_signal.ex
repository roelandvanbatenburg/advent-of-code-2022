defmodule DistressSignal do
  @moduledoc """
  Read the signal.
  """
  defmodule PartOne do
    @moduledoc "First puzzle."
    @doc "Solve the first puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> DistressSignal.parse()
      |> Enum.filter(fn %{left: left, right: right} -> DistressSignal.ordered?(left, right) end)
      |> Enum.reduce(0, fn packet_pair, sum -> sum + packet_pair.index end)
    end
  end

  defmodule PartTwo do
    @moduledoc "Second puzzle."

    @packet1 [[2]]
    @packet2 [[6]]

    @doc "Solve the second puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> DistressSignal.parse()
      |> Enum.flat_map(fn %{left: left, right: right} -> [left, right] end)
      |> Enum.concat([@packet1, @packet2])
      |> Enum.sort(&DistressSignal.ordered?/2)
      |> decoder_key()
    end

    defp decoder_key(packets) do
      (Enum.find_index(packets, &Kernel.==(&1, @packet1)) + 1) *
        (Enum.find_index(packets, &Kernel.==(&1, @packet2)) + 1)
    end
  end

  @type packet :: integer() | [packet()]
  @type packet_pair :: %{left: packet(), right: packet(), index: non_neg_integer()}

  @doc "Parse the packets."
  @spec parse(String.t()) :: [packet_pair()]
  def parse(input) do
    input
    |> String.split("\n\n")
    |> Enum.with_index(1)
    |> Enum.map(fn {lines, index} ->
      [left, right] = String.split(lines, "\n")
      %{left: eval(left), right: eval(right), index: index}
    end)
  end

  defp eval(input) do
    {result, _bindings} = Code.eval_string(input)
    result
  end

  @doc "Check if left comes before right."
  @spec ordered?(packet(), packet()) :: boolean()
  def ordered?(left, right) do
    longest = max(length(left), length(right)) - 1

    0..longest
    |> Enum.reduce_while(:undecided, fn index, _acc ->
      l = Enum.at(left, index, :empty)
      r = Enum.at(right, index, :empty)

      {l, r}
      |> check()
    end)
  end

  defp check({:empty, :empty}), do: halt_or_continue(:undecided)
  defp check({:empty, _r}), do: halt_or_continue(true)
  defp check({_l, :empty}), do: halt_or_continue(false)

  defp check({l, r}) when is_integer(l) and is_integer(r) do
    if l < r do
      halt_or_continue(true)
    else
      if l > r do
        halt_or_continue(false)
      else
        halt_or_continue(:undecided)
      end
    end
  end

  defp check({l, r}) when is_list(l) and is_list(r), do: ordered?(l, r) |> halt_or_continue()
  defp check({l, r}) when is_integer(l), do: ordered?([l], r) |> halt_or_continue()
  defp check({l, r}) when is_integer(r), do: ordered?(l, [r]) |> halt_or_continue()

  defp halt_or_continue(:undecided), do: {:cont, :undecided}
  defp halt_or_continue(result), do: {:halt, result}
end
