defmodule SupplyStacks do
  @moduledoc """
  Reorganize the stacks.
  """
  defmodule PartOne do
    @moduledoc "First puzzle."
    @doc "Solve the first puzzle."
    @spec run(String.t()) :: String.t()
    def run(input) do
      input
      |> SupplyStacks.parse()
      |> execute()
      |> SupplyStacks.top_crates()
      |> Enum.join()
    end

    defp execute({stacks, instructions}) do
      instructions
      |> Enum.reduce(stacks, fn {count, from, to}, stacks ->
        1..count
        |> Enum.reduce(stacks, fn _i, stacks ->
          move_crate(stacks, from, to)
        end)
      end)
    end

    defp move_crate(stacks, from, to) do
      from = SupplyStacks.find_first_crate(stacks, from)
      {crate, stacks} = Map.pop!(stacks, from)
      to = SupplyStacks.find_first_empty(stacks, to)
      Map.put(stacks, to, crate)
    end
  end

  defmodule PartTwo do
    @moduledoc "Second puzzle."
    @doc "Solve the second puzzle."
    @spec run(String.t()) :: String.t()
    def run(input) do
      input
      |> SupplyStacks.parse()
      |> execute()
      |> SupplyStacks.top_crates()
      |> Enum.join()
    end

    defp execute({stacks, instructions}) do
      instructions
      |> Enum.reduce(stacks, fn {count, from, to}, stacks ->
        move_crates(stacks, count, from, to)
      end)
    end

    defp move_crates(stacks, count, from, to) do
      {_from_col, from_row} = SupplyStacks.find_first_crate(stacks, from)
      {_to_col, to_row} = SupplyStacks.find_first_empty(stacks, to)

      0..(count - 1)
      |> Enum.reduce(stacks, fn i, stacks ->
        {crate, stacks} = Map.pop!(stacks, {from, from_row + i})
        Map.put(stacks, {to, to_row - count + i + 1}, crate)
      end)
    end
  end

  @doc "Parse the input"
  @spec parse(binary) :: {%{}, [{integer, integer, integer}]}
  def parse(input) do
    [stacks, instructions] = String.split(input, "\n\n")
    {parse_stacks(stacks), parse_instructions(instructions)}
  end

  defp parse_stacks(stacks) do
    parsed_stacks =
      stacks
      |> String.split("\n")
      |> List.delete_at(-1)
      |> Enum.map(fn line ->
        line
        |> Kernel.<>(" ")
        |> String.codepoints()
        |> Enum.chunk_every(4)
        |> Enum.map(&parse_crate/1)
      end)

    {crates, _row_idx} =
      parsed_stacks
      |> Enum.reduce({%{}, 0}, fn row, {crates, row_idx} ->
        {crates, row_idx, _col_idx} =
          row
          |> Enum.reduce({crates, row_idx, 1}, fn
            nil, {crates, row_idex, col_idx} ->
              {crates, row_idex, col_idx + 1}

            crate, {crates, row_idx, col_idx} ->
              {Map.put(crates, {col_idx, row_idx}, crate), row_idx, col_idx + 1}
          end)

        {crates, row_idx + 1}
      end)

    crates
  end

  defp parse_crate([_, " ", _, _]), do: nil
  defp parse_crate(["[", letter, "]", " "]), do: letter

  defp parse_instructions(instructions) do
    instructions
    |> String.split("\n")
    |> Enum.map(&parse_instruction/1)
  end

  defp parse_instruction(instruction) do
    ["move", count, "from", from, "to", to] = String.split(instruction)
    {String.to_integer(count), String.to_integer(from), String.to_integer(to)}
  end

  @doc "Return the top crates."
  @spec top_crates(any) :: [String.t()]
  def top_crates(stacks) do
    stacks
    |> Enum.group_by(fn {{col, _row}, _crate} -> col end, fn {{_col, row}, crate} ->
      {row, crate}
    end)
    |> Enum.map(fn {_col, row_with_crates} ->
      row_with_crates
      |> Enum.sort()
      |> List.first()
      |> elem(1)
    end)
  end

  @doc "Return location of the first create in col"
  @spec find_first_crate(map(), integer()) :: {integer, integer}
  def find_first_crate(stacks, col) do
    stacks
    |> Map.keys()
    |> Enum.filter(&(elem(&1, 0) == col))
    |> Enum.sort()
    |> List.first()
  end

  @doc "Return first empty location in col"
  @spec find_first_empty(map(), integer()) :: {integer, integer}
  def find_first_empty(stacks, col) do
    stacks
    |> Map.keys()
    |> Enum.filter(&(elem(&1, 0) == col))
    |> Enum.sort()
    |> List.first()
    |> case do
      {col, row} -> {col, row - 1}
      nil -> {col, 0}
    end
  end
end
