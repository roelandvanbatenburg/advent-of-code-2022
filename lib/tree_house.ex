defmodule TreeHouse do
  @moduledoc """
  Make a tree house.
  """
  defmodule PartOne do
    @moduledoc "First puzzle."
    @doc "Solve the first puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> TreeHouse.parse()
      |> visible_trees()
    end

    defp visible_trees({trees, {max_y, last_x}}) do
      max_x = last_x - 1

      trees
      |> Enum.reduce(0, fn {location, height}, visible_trees ->
        if visible?(trees, location, height, {max_y, max_x}) do
          visible_trees + 1
        else
          visible_trees
        end
      end)
    end

    defp visible?(_trees, {y, _x}, _height, {y, _max_x}), do: true
    defp visible?(_trees, {0, _x}, _height, {_max_y, _max_x}), do: true
    defp visible?(_trees, {_y, x}, _height, {_max_y, x}), do: true
    defp visible?(_trees, {_y, 0}, _height, {_max_y, _max_x}), do: true

    defp visible?(trees, {y, x}, height, {max_y, max_x}) do
      visible_left?(trees, y, x, height) or
        visible_right?(trees, y, x, height, max_x) or
        visible_top?(trees, y, x, height) or
        visible_bottom?(trees, y, x, height, max_y)
    end

    defp visible_left?(trees, y, x, height),
      do: Enum.all?(0..(x - 1), &(Map.fetch!(trees, {y, &1}) < height))

    defp visible_right?(trees, y, x, height, max_x),
      do: Enum.all?(max_x..(x + 1), &(Map.fetch!(trees, {y, &1}) < height))

    defp visible_top?(trees, y, x, height),
      do: Enum.all?(0..(y - 1), &(Map.fetch!(trees, {&1, x}) < height))

    defp visible_bottom?(trees, y, x, height, max_y),
      do: Enum.all?(max_y..(y + 1), &(Map.fetch!(trees, {&1, x}) < height))
  end

  defmodule PartTwo do
    @moduledoc "Second puzzle."
    @doc "Solve the second puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> TreeHouse.parse()
      |> max_scenic_score()
    end

    defp max_scenic_score({trees, {max_y, last_x}}) do
      max_x = last_x - 1

      trees
      |> Enum.map(&scenic_score(trees, &1, max_y, max_x))
      |> Enum.max()
    end

    defp scenic_score(trees, {{y, x}, height}, max_y, max_x) do
      score_left(trees, y, x, height) * score_right(trees, y, x, height, max_x) *
        score_top(trees, y, x, height) * score_bottom(trees, y, x, height, max_y)
    end

    defp score_left(_trees, _y, 0, _height), do: 0

    defp score_left(trees, y, x, height) do
      1..x
      |> Enum.find(x, &(Map.fetch!(trees, {y, x - &1}) >= height))
    end

    defp score_right(_trees, _y, x, _height, x), do: 0

    defp score_right(trees, y, x, height, max_x) do
      1..(max_x - x)
      |> Enum.find(max_x - x, &(Map.fetch!(trees, {y, x + &1}) >= height))
    end

    defp score_top(_trees, 0, _x, _height), do: 0

    defp score_top(trees, y, x, height) do
      1..y
      |> Enum.find(y, &(Map.fetch!(trees, {y - &1, x}) >= height))
    end

    defp score_bottom(_trees, y, _x, _height, y), do: 0

    defp score_bottom(trees, y, x, height, max_y) do
      1..(max_y - y)
      |> Enum.find(max_y - y, &(Map.fetch!(trees, {y + &1, x}) >= height))
    end
  end

  @doc "Parse the trees."
  @spec parse(String.t()) :: {map(), {integer(), integer()}}
  def parse(input) do
    input
    |> String.graphemes()
    |> Enum.reduce({%{}, {0, 0}}, fn token, {trees, {y, x}} ->
      case token do
        "\n" -> {trees, {y + 1, 0}}
        tree -> {Map.put(trees, {y, x}, String.to_integer(tree)), {y, x + 1}}
      end
    end)
  end
end
