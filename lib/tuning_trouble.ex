defmodule TuningTrouble do
  @moduledoc """
  Tune in.
  """
  defmodule PartOne do
    @moduledoc "First puzzle."
    @doc "Solve the first puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> TuningTrouble.find_end_of_non_repeating_pattern(4)
    end
  end

  defmodule PartTwo do
    @moduledoc "Second puzzle."
    @doc "Solve the second puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> TuningTrouble.find_end_of_non_repeating_pattern(14)
    end
  end

  @doc "Find the end of the first non-repeating pattern with `length` in `input`."
  @spec find_end_of_non_repeating_pattern(String.t(), integer()) :: integer()
  def find_end_of_non_repeating_pattern(input, length) do
    0..(String.length(input) - (length - 1))
    |> Enum.find(fn index ->
      input
      |> String.slice(index, length)
      |> String.codepoints()
      |> Enum.uniq()
      |> length()
      |> Kernel.==(length)
    end)
    |> Kernel.+(length)
  end
end
