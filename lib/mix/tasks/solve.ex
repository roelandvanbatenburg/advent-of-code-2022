defmodule Mix.Tasks.Solve do
  use Mix.Task

  @moduledoc """
  Solve a specific task for a day

  ## Example

  mix solve 01 (--part2)
  """

  @doc """
  Run puzzle specified by args
  """
  @spec run([String.t()]) :: :ok
  def run([number | args]) do
    "priv/input_#{number}.txt"
    |> File.stream!()
    |> Stream.map(&String.trim_trailing/1)
    |> Enum.join("\n")
    |> run(number, args)
    |> maybe_to_string()
    |> Mix.shell().info()
  end

  defp run(input, number, args) do
    [module(number), submodule(args)]
    |> Module.concat()
    |> apply(:run, [input])
  end

  defp module("01"), do: CaloryCounting
  defp module("02"), do: RockPaperScissors
  defp module("03"), do: RucksackReorganization
  defp module("04"), do: CampCleanup
  defp module("05"), do: SupplyStacks
  defp module("06"), do: TuningTrouble
  defp module("07"), do: NoSpace
  defp module("08"), do: TreeHouse
  defp module("09"), do: RopeBridge
  defp module("10"), do: CathodeRay
  defp module("11"), do: Monkeys
  defp module("12"), do: HillClimbing
  defp module("13"), do: DistressSignal
  defp module("14"), do: RegolithReservoir

  defp submodule([]), do: PartOne
  defp submodule(_), do: PartTwo

  defp maybe_to_string(output) when is_integer(output), do: Integer.to_string(output)
  defp maybe_to_string(output), do: output
end
