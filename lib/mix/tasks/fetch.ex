defmodule Mix.Tasks.Fetch do
  use Mix.Task

  @moduledoc """
  Fetch input

  ## Example

  echo 'config :advent_of_code_2022, session: "<your cookie>"' >> config/config.exs
  mix fetch 01
  """

  @doc """
  Run puzzle specified by args
  """
  @spec run([String.t()]) :: :ok
  def run([number]) do
    path = "https://adventofcode.com/2022/day/#{String.trim_leading(number, "0")}/input"
    {:ok, {{'HTTP/1.1', 200, 'OK'}, _, input}} = :httpc.request(:get, {path, headers()}, [], [])
    :ok = File.write("priv/input_#{number}.txt", input)
  end

  defp headers() do
    session = Application.get_env(:advent_of_code_2022, :session)
    [{'cookie', String.to_charlist("session=#{session}")}]
  end
end
