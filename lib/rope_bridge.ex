defmodule RopeBridge do
  @moduledoc """
  Follow the rope.
  """
  defmodule PartOne do
    @moduledoc "First puzzle."
    @doc "Solve the first puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> RopeBridge.parse()
      |> follow_the_head()
    end

    defp follow_the_head(directions) do
      {_head, _tail, locations} =
        directions
        |> Enum.reduce(
          {{0, 0}, {0, 0}, MapSet.new([{0, 0}])},
          fn direction, {head, tail, locations} ->
            head = RopeBridge.move_head(head, direction)
            tail = RopeBridge.move_tail(tail, head)
            locations = MapSet.put(locations, tail)
            {head, tail, locations}
          end
        )

      MapSet.size(locations)
    end
  end

  defmodule PartTwo do
    @moduledoc "Second puzzle."
    @doc "Solve the second puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> RopeBridge.parse()
      |> follow_the_head()
    end

    defp follow_the_head(directions) do
      {_head, _tail, locations} =
        directions
        |> Enum.reduce(
          {{0, 0}, List.duplicate({0, 0}, 9), MapSet.new([{0, 0}])},
          fn direction, {head, knots, locations} ->
            head = RopeBridge.move_head(head, direction)

            # Calculate the new location in for each tail.
            # The accumulator holds a (reversed) list of knots and the current last updated knot.
            {knots, tail} =
              Enum.reduce(knots, {[], head}, fn tail, {knots, head} ->
                tail = RopeBridge.move_tail(tail, head)
                {[tail | knots], tail}
              end)

            locations = MapSet.put(locations, tail)
            {head, Enum.reverse(knots), locations}
          end
        )

      MapSet.size(locations)
    end
  end

  @type direction :: :right | :left | :up | :down
  @type location :: {integer(), integer()}

  @doc "Parse the instructions."
  @spec parse(String.t()) :: [direction()]
  def parse(input) do
    input
    |> String.split("\n")
    |> Enum.flat_map(fn <<direction::binary-size(1)>> <> " " <> count ->
      List.duplicate(parse_direction(direction), String.to_integer(count))
    end)
  end

  defp parse_direction("U"), do: :up
  defp parse_direction("D"), do: :down
  defp parse_direction("R"), do: :right
  defp parse_direction("L"), do: :left

  @doc "Move `head` toward `direction` and return new location."
  @spec move_head(location(), direction()) :: location()
  def move_head(head, direction)
  def move_head({y, x}, :up), do: {y + 1, x}
  def move_head({y, x}, :down), do: {y - 1, x}
  def move_head({y, x}, :right), do: {y, x + 1}
  def move_head({y, x}, :left), do: {y, x - 1}

  @doc "Move `tail` toward `head` and return new location."
  @spec move_tail(location(), location()) :: location()
  def move_tail({y_tail, x_tail} = tail, {y_head, x_head} = _head) do
    y_diff = y_head - y_tail
    x_diff = x_head - x_tail

    if abs(y_diff) < 2 and abs(x_diff) < 2 do
      {y_tail, x_tail}
    else
      case abs(y_diff) + abs(x_diff) do
        2 -> move_tower(y_diff, x_diff, tail)
        3 -> move_knight(y_diff, x_diff, tail)
        4 -> move_bishop(y_diff, x_diff, tail)
      end
    end
  end

  defp move_tower(0, x_diff, {y, x}) when x_diff < 0, do: {y, x - 1}
  defp move_tower(0, _x_diff, {y, x}), do: {y, x + 1}
  defp move_tower(y_diff, 0, {y, x}) when y_diff < 0, do: {y - 1, x}
  defp move_tower(_y_diff, 0, {y, x}), do: {y + 1, x}

  defp move_knight(y_diff, x_diff, {y, x}) when x_diff < 0 and y_diff < 0, do: {y - 1, x - 1}
  defp move_knight(y_diff, x_diff, {y, x}) when x_diff > 0 and y_diff < 0, do: {y - 1, x + 1}
  defp move_knight(_y_diff, x_diff, {y, x}) when x_diff < 0, do: {y + 1, x - 1}
  defp move_knight(_y_diff, x_diff, {y, x}) when x_diff > 0, do: {y + 1, x + 1}

  defp move_bishop(2, 2, {y, x}), do: {y + 1, x + 1}
  defp move_bishop(2, -2, {y, x}), do: {y + 1, x - 1}
  defp move_bishop(-2, 2, {y, x}), do: {y - 1, x + 1}
  defp move_bishop(-2, -2, {y, x}), do: {y - 1, x - 1}
end
