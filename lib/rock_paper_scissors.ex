defmodule RockPaperScissors do
  @moduledoc """
  Win the game
  """
  defmodule PartOne do
    @moduledoc "First puzzle"
    @doc "Solve the first puzzle"
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> RockPaperScissors.strategy_guide()
      |> Enum.reduce(0, fn play, score -> score + score(play) end)
    end

    defp score({:A, :X}), do: 1 + 3
    defp score({:A, :Y}), do: 2 + 6
    defp score({:A, :Z}), do: 3 + 0

    defp score({:B, :X}), do: 1 + 0
    defp score({:B, :Y}), do: 2 + 3
    defp score({:B, :Z}), do: 3 + 6

    defp score({:C, :X}), do: 1 + 6
    defp score({:C, :Y}), do: 2 + 0
    defp score({:C, :Z}), do: 3 + 3
  end

  defmodule PartTwo do
    @moduledoc "Second puzzle"
    @doc "Solve the second puzzle"
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> RockPaperScissors.strategy_guide()
      |> Enum.reduce(0, fn play, score -> score + score(play) end)
    end

    defp score({:A, :X}), do: 3 + 0
    defp score({:A, :Y}), do: 1 + 3
    defp score({:A, :Z}), do: 2 + 6

    defp score({:B, :X}), do: 1 + 0
    defp score({:B, :Y}), do: 2 + 3
    defp score({:B, :Z}), do: 3 + 6

    defp score({:C, :X}), do: 2 + 0
    defp score({:C, :Y}), do: 3 + 3
    defp score({:C, :Z}), do: 1 + 6
  end

  @doc "Return a strategy guide"
  # Rock, Paper, Scissors
  @type opponent_value :: :A | :B | :C
  # Rock, Paper, Scissors or
  # Lose, Draw, Win
  @type response_value :: :X | :Y | :Z
  @spec strategy_guide(String.t()) :: [{opponent_value(), response_value()}]
  def strategy_guide(input) do
    input
    |> String.split("\n")
    |> Enum.map(fn line ->
      [opponent, response] = String.split(line, " ")
      {String.to_existing_atom(opponent), String.to_existing_atom(response)}
    end)
  end
end
