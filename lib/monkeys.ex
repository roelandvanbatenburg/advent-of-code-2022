defmodule Monkeys do
  @moduledoc """
  Get your items back.
  """
  defmodule PartOne do
    @moduledoc "First puzzle."
    @doc "Solve the first puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> Monkeys.parse()
      |> Monkeys.play(20, 3)
      |> Monkeys.level()
    end
  end

  defmodule PartTwo do
    @moduledoc "Second puzzle."
    @doc "Solve the second puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> Monkeys.parse()
      |> Monkeys.play(10_000, :lcm)
      |> Monkeys.level()
    end
  end

  @type monkey :: %{
          items: [integer()],
          inspect: (integer() -> integer()),
          test_div: integer(),
          targets: %{true: integer(), false: integer()},
          lcm: integer()
        }
  @type monkey_map :: %{required(integer()) => monkey()}

  @doc "Parse the monkeys."
  @spec parse(String.t()) :: monkey_map()
  def parse(input) do
    input
    |> String.split("\n\n")
    |> Enum.map(&parse_monkey/1)
    |> add_lcm()
    |> Map.new()
  end

  defp parse_monkey(input) do
    [
      "Monkey " <> <<id::bytes-size(1)>> <> ":",
      "  Starting items: " <> items,
      "  Operation: new = " <> operation,
      "  Test: divisible by " <> test_div,
      "    If true: throw to monkey " <> true_target,
      "    If false: throw to monkey " <> false_target
    ] = String.split(input, "\n")

    {String.to_integer(id),
     %{
       items: items |> String.split(", ") |> Enum.map(&String.to_integer/1),
       inspect: parse_operation(operation),
       test_div: String.to_integer(test_div),
       targets: %{true: String.to_integer(true_target), false: String.to_integer(false_target)},
       inspections: 0
     }}
  end

  defp parse_operation(operation) do
    [input1, operator, input2] = String.split(operation)

    case operator do
      "+" -> fn item -> Kernel.+(parse_input(input1, item), parse_input(input2, item)) end
      "*" -> fn item -> Kernel.*(parse_input(input1, item), parse_input(input2, item)) end
    end
  end

  defp parse_input("old", item), do: item
  defp parse_input(i, _item), do: String.to_integer(i)

  defp add_lcm(monkeys) do
    lcm =
      monkeys
      |> Enum.map(fn {_id, monkey} -> Map.fetch!(monkey, :test_div) end)
      |> Enum.product()

    monkeys
    |> Enum.map(fn {id, monkey} -> {id, Map.put(monkey, :lcm, lcm)} end)
  end

  @doc """
  Play `rounds` with the `monkeys`, using `reducer` to reduce the worry.
  """
  @spec play(monkey_map(), integer(), :lcm | integer()) :: monkey_map()
  def play(monkeys, 0, _reducer), do: monkeys

  def play(monkeys, rounds, reducer) do
    monkeys
    |> Enum.reduce(monkeys, fn {id, _monkey}, monkeys ->
      throw_items(id, monkeys, reducer)
    end)
    |> play(rounds - 1, reducer)
  end

  defp throw_items(id, monkeys, reducer) do
    monkey = Map.fetch!(monkeys, id)

    monkey.items
    |> Enum.reduce(monkeys, fn item, monkeys ->
      item =
        item
        |> monkey.inspect.()
        |> reduce(reducer, monkey)

      target = Map.get(monkey.targets, rem(item, monkey.test_div) == 0)

      monkeys
      |> throw_item_to_monkey(target, item)
      |> add_inspection(id)
    end)
    |> remove_items(id)
  end

  defp reduce(item, :lcm, %{lcm: lcm}), do: Kernel.rem(item, lcm)
  defp reduce(item, reducer, _monkey), do: Kernel.div(item, reducer)

  defp throw_item_to_monkey(monkeys, target, item) do
    monkeys
    |> Map.update!(target, fn monkey ->
      Map.update!(monkey, :items, &Kernel.++(&1, [item]))
    end)
  end

  defp add_inspection(monkeys, id) do
    {_i, monkeys} = get_and_update_in(monkeys, [id, :inspections], &{&1, Kernel.+(&1, 1)})
    monkeys
  end

  defp remove_items(monkeys, id) do
    put_in(monkeys, [id, :items], [])
  end

  @doc """
  Determine the level of monkey business.
  """
  @spec level(monkey_map()) :: integer()
  def level(monkeys) do
    monkeys
    |> Enum.map(fn {_id, monkey} -> Map.fetch!(monkey, :inspections) end)
    |> Enum.sort(:desc)
    |> Enum.take(2)
    |> Enum.product()
  end
end
