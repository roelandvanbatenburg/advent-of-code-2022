defmodule CampCleanup do
  @moduledoc """
  Clean up the camp.
  """
  defmodule PartOne do
    @moduledoc "First puzzle."
    @doc "Solve the first puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> String.split()
      |> Enum.map(&CampCleanup.parse/1)
      |> Enum.filter(&overlap?/1)
      |> length()
    end

    defp overlap?({{min1, max1}, {min2, max2}}) when min1 <= min2 and max1 >= max2, do: true
    defp overlap?({{min1, max1}, {min2, max2}}) when min2 <= min1 and max2 >= max1, do: true
    defp overlap?(_), do: false
  end

  defmodule PartTwo do
    @moduledoc "Second puzzle."
    @doc "Solve the second puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> String.split()
      |> Enum.map(&CampCleanup.parse/1)
      |> Enum.filter(&overlap?/1)
      |> length()
    end

    defp overlap?({{min1, max1}, {min2, max2}}) do
      Range.new(min1, max1)
      |> Range.disjoint?(Range.new(min2, max2))
      |> Kernel.not()
    end
  end

  @doc "Parse a single line into two definitions section definitions."
  @spec parse(String.t()) :: {{integer(), integer()}, {integer(), integer()}}
  def parse(input) do
    [e1, e2] = String.split(input, ",")
    {parse_range(e1), parse_range(e2)}
  end

  defp parse_range(input) do
    [min, max] = String.split(input, "-")
    {String.to_integer(min), String.to_integer(max)}
  end
end
