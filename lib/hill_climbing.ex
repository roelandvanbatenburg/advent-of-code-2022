defmodule HillClimbing do
  @moduledoc """
  Reach the top.
  """
  defmodule PartOne do
    @moduledoc "First puzzle."
    @doc "Solve the first puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      input
      |> HillClimbing.parse()
      |> find_route_to_top()
    end

    defp find_route_to_top({heights, start, finish}) do
      MapSet.new([[start]])
      |> HillClimbing.route_length_to_top(heights, finish)
    end
  end

  defmodule PartTwo do
    @moduledoc "Second puzzle."
    @doc "Solve the second puzzle."
    @spec run(String.t()) :: integer()
    def run(input) do
      {heights, _start, finish} = HillClimbing.parse(input)

      heights
      |> Enum.filter(&Kernel.==(elem(&1, 1), 0))
      |> Enum.reduce(MapSet.new(), fn {start, _height}, routes -> MapSet.put(routes, [start]) end)
      |> HillClimbing.route_length_to_top(heights, finish)
    end
  end

  @type location :: {integer, integer}
  @type heights :: %{required(location()) => integer()}

  @doc "Parse the height_map."
  @spec parse(String.t()) :: {heights(), location(), location()}
  def parse(input) do
    {heights, start, finish, _y} =
      input
      |> String.split("\n")
      |> Enum.reduce({%{}, nil, nil, 0}, fn line, {heights, start, finish, y} ->
        parse_line(line, heights, start, finish, y)
      end)

    {heights, start, finish}
  end

  defp parse_line(line, heights, start, finish, y) do
    {heights, start, finish, y, _x} =
      line
      |> String.graphemes()
      |> Enum.reduce({heights, start, finish, y, 0}, fn height, {heights, start, finish, y, x} ->
        case height(height) do
          :start -> {Map.put(heights, {y, x}, height("a")), {y, x}, finish, y, x + 1}
          :finish -> {Map.put(heights, {y, x}, height("z")), start, {y, x}, y, x + 1}
          height -> {Map.put(heights, {y, x}, height), start, finish, y, x + 1}
        end
      end)

    {heights, start, finish, y + 1}
  end

  defp height(<<h::utf8>>) when h in ?a..?z, do: h - ?a
  defp height("S"), do: :start
  defp height("E"), do: :finish

  @doc "Length of the shortest route to the top."
  @spec route_length_to_top(MapSet.t([location()]), heights(), location()) :: non_neg_integer()
  def route_length_to_top(routes, heights, finish) do
    routes
    |> filter()
    |> Enum.reduce_while(MapSet.new(), fn route, candidates ->
      {:cont, candidates}
      |> add_candidate(route, heights, finish, :left)
      |> add_candidate(route, heights, finish, :right)
      |> add_candidate(route, heights, finish, :up)
      |> add_candidate(route, heights, finish, :down)
    end)
    |> case do
      {:finish, length} -> length
      routes -> route_length_to_top(routes, heights, finish)
    end
  end

  defp filter(routes) do
    routes
    |> Enum.uniq_by(&List.first/1)
  end

  defp add_candidate({:halt, acc}, _route, _heights, _finish, _direction), do: {:halt, acc}

  defp add_candidate(
         {:cont, candidates},
         [current_location | previous_locations] = route,
         heights,
         finish,
         direction
       ) do
    next_location = next_location(current_location, direction)

    if valid?(
         Map.get(heights, next_location),
         Map.get(heights, current_location),
         next_location,
         previous_locations
       ) do
      if next_location == finish do
        {:halt, {:finish, length(route)}}
      else
        {:cont, MapSet.put(candidates, [next_location | route])}
      end
    else
      {:cont, candidates}
    end
  end

  defp next_location({y, x}, :left), do: {y, x - 1}
  defp next_location({y, x}, :right), do: {y, x + 1}
  defp next_location({y, x}, :up), do: {y - 1, x}
  defp next_location({y, x}, :down), do: {y + 1, x}

  defp valid?(nil, _current_height, _next_location, _previous_locations), do: false
  defp valid?(next_height, height, _n, _ps) when next_height > height + 1, do: false
  defp valid?(_next_height, _current_height, next, prevs), do: not Enum.member?(prevs, next)
end
