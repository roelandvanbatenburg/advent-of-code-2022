defmodule HillClimbingTest do
  use ExUnit.Case

  alias HillClimbing.PartOne
  alias HillClimbing.PartTwo

  setup _context do
    %{
      input:
        """
        Sabqponm
        abcryxxl
        accszExk
        acctuvwj
        abdefghi
        """
        |> String.trim_trailing()
    }
  end

  test "part one", %{input: input} do
    assert 31 == PartOne.run(input)
  end

  test "part two", %{input: input} do
    assert 29 == PartTwo.run(input)
  end
end
