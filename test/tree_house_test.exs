defmodule TreeHouseTest do
  use ExUnit.Case

  alias TreeHouse.PartOne
  alias TreeHouse.PartTwo

  setup _context do
    %{
      input:
        """
        30373
        25512
        65332
        33549
        35390
        """
        |> String.trim_trailing()
    }
  end

  test "part one", %{input: input} do
    assert 21 == PartOne.run(input)
  end

  test "part two", %{input: input} do
    assert 8 == PartTwo.run(input)
  end
end
