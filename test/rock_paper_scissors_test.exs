defmodule RockPaperScissorsTest do
  use ExUnit.Case

  alias RockPaperScissors.PartOne
  alias RockPaperScissors.PartTwo

  setup _context do
    %{
      input:
        """
        A Y
        B X
        C Z
        """
        |> String.trim()
    }
  end

  test "part one", %{input: input} do
    assert 15 == PartOne.run(input)
  end

  test "part two", %{input: input} do
    assert 12 == PartTwo.run(input)
  end
end
