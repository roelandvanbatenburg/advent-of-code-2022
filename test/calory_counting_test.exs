defmodule CaloryCountingTest do
  use ExUnit.Case

  alias CaloryCounting.PartOne
  alias CaloryCounting.PartTwo

  setup _context do
    %{
      input:
        """
        1000
        2000
        3000

        4000

        5000
        6000

        7000
        8000
        9000

        10000
        """
        |> String.trim()
    }
  end

  test "part one", %{input: input} do
    assert 24_000 == PartOne.run(input)
  end

  test "part two", %{input: input} do
    assert 45_000 == PartTwo.run(input)
  end
end
