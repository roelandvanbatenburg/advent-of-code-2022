defmodule DistressSignalTest do
  use ExUnit.Case

  alias DistressSignal.PartOne
  alias DistressSignal.PartTwo

  setup _context do
    %{
      input:
        """
        [1,1,3,1,1]
        [1,1,5,1,1]

        [[1],[2,3,4]]
        [[1],4]

        [9]
        [[8,7,6]]

        [[4,4],4,4]
        [[4,4],4,4,4]

        [7,7,7,7]
        [7,7,7]

        []
        [3]

        [[[]]]
        [[]]

        [1,[2,[3,[4,[5,6,7]]]],8,9]
        [1,[2,[3,[4,[5,6,0]]]],8,9]
        """
        |> String.trim_trailing()
    }
  end

  test "part one", %{input: input} do
    assert 13 == PartOne.run(input)
  end

  test "part two", %{input: input} do
    assert 140 == PartTwo.run(input)
  end
end
