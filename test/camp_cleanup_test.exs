defmodule CampCleanupTest do
  use ExUnit.Case

  alias CampCleanup.PartOne
  alias CampCleanup.PartTwo

  setup _context do
    %{
      input:
        """
        2-4,6-8
        2-3,4-5
        5-7,7-9
        2-8,3-7
        6-6,4-6
        2-6,4-8
        """
        |> String.trim()
    }
  end

  test "part one", %{input: input} do
    assert 2 == PartOne.run(input)
  end

  test "part two", %{input: input} do
    assert 4 == PartTwo.run(input)
  end
end
