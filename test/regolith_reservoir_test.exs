defmodule RegolithReservoirTest do
  use ExUnit.Case

  alias RegolithReservoir.PartOne
  alias RegolithReservoir.PartTwo

  setup _context do
    %{
      input:
        """
        498,4 -> 498,6 -> 496,6
        503,4 -> 502,4 -> 502,9 -> 494,9
        """
        |> String.trim_trailing()
    }
  end

  test "part one", %{input: input} do
    assert 24 == PartOne.run(input)
  end

  test "part two", %{input: input} do
    assert 93 == PartTwo.run(input)
  end
end
