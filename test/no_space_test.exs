defmodule NoSpaceTest do
  use ExUnit.Case

  alias NoSpace.PartOne
  alias NoSpace.PartTwo

  setup _context do
    %{
      input:
        """
        $ cd /
        $ ls
        dir a
        14848514 b.txt
        8504156 c.dat
        dir d
        $ cd a
        $ ls
        dir e
        29116 f
        2557 g
        62596 h.lst
        $ cd e
        $ ls
        584 i
        $ cd ..
        $ cd ..
        $ cd d
        $ ls
        4060174 j
        8033020 d.log
        5626152 d.ext
        7214296 k
        """
        |> String.trim_trailing()
    }
  end

  test "part one", %{input: input} do
    assert 95_437 == PartOne.run(input)
  end

  test "part two", %{input: input} do
    assert 24_933_642 == PartTwo.run(input)
  end
end
