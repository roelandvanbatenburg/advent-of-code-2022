defmodule TuningTroubleTest do
  use ExUnit.Case

  alias TuningTrouble.PartOne
  alias TuningTrouble.PartTwo

  setup _context do
    %{input: "mjqjpqmgbljsphdztnvjfqwrcgsmlb"}
  end

  test "part one", %{input: input} do
    assert 7 == PartOne.run(input)
  end

  test "part two", %{input: input} do
    assert 19 == PartTwo.run(input)
  end
end
