defmodule RopeBridgeTest do
  use ExUnit.Case

  alias RopeBridge.PartOne
  alias RopeBridge.PartTwo

  setup _context do
    %{
      input:
        """
        R 4
        U 4
        L 3
        D 1
        R 4
        D 1
        L 5
        R 2
        """
        |> String.trim_trailing()
    }
  end

  test "part one", %{input: input} do
    assert 13 == PartOne.run(input)
  end

  test "part two", %{input: input} do
    assert 1 == PartTwo.run(input)

    assert 36 ==
             """
             R 5
             U 8
             L 8
             D 3
             R 17
             D 10
             L 25
             U 20
             """
             |> String.trim_trailing()
             |> PartTwo.run()
  end
end
