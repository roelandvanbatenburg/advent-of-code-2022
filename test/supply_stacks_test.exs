defmodule SupplyStacksTest do
  use ExUnit.Case

  alias SupplyStacks.PartOne
  alias SupplyStacks.PartTwo

  setup _context do
    %{
      input:
        [
          "    [D]    ",
          "[N] [C]    ",
          "[Z] [M] [P]",
          "1   2   3 "
        ]
        |> Enum.join("\n")
        |> Kernel.<>("""


        move 1 from 2 to 1
        move 3 from 1 to 3
        move 2 from 2 to 1
        move 1 from 1 to 2
        """)
        |> String.trim_trailing()
    }
  end

  test "part one", %{input: input} do
    assert "CMZ" == PartOne.run(input)
  end

  test "part two", %{input: input} do
    assert "MCD" == PartTwo.run(input)
  end
end
