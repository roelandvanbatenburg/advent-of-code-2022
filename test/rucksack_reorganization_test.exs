defmodule RucksackReorganizationTest do
  use ExUnit.Case

  alias RucksackReorganization.PartOne
  alias RucksackReorganization.PartTwo

  setup _context do
    %{
      input:
        """
        vJrwpWtwJgWrhcsFMMfFFhFp
        jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
        PmmdzqPrVvPwwTWBwg
        wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
        ttgJtRGJQctTZtZT
        CrZsJsPPZsGzwwsLwLmpwMDw
        """
        |> String.trim()
    }
  end

  test "part one", %{input: input} do
    assert 157 == PartOne.run(input)
  end

  test "part two", %{input: input} do
    assert 70 == PartTwo.run(input)
  end
end
